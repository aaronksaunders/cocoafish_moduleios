//
//  CocoafishProxy.m
//  cocoafishmodule
//
//  Created by Aaron Saunders on 12/23/11.
//  Copyright (c) 2011 Clearly Innovative Inc. All rights reserved.
//

#import "CocoafishMgrProxy.h"
#import "Mimetypes.h"

#import "Cocoafish.h"


@implementation CocoafishMgrProxy

- (id)initWithTiApp:(TiApp *)tiApp keys:(NSDictionary *)keys
{
    self = [super init];
    app = tiApp;
    
    RELEASE_TO_NIL(consumer_key_value);
    RELEASE_TO_NIL(secret_key_value);
    RELEASE_TO_NIL(fb_key_value);
    
    consumer_key_value = [[TiUtils stringValue:[keys objectForKey:@"consumer_key"]] retain];
    secret_key_value = [[TiUtils stringValue:[keys objectForKey:@"secret_key"]] retain];
    fb_key_value = [[TiUtils stringValue:[keys objectForKey:@"facebookAppId"]] retain];

    NSMutableDictionary *appKeys = [NSMutableDictionary dictionary];

    if ( fb_key_value != nil) {
        [appKeys setObject:fb_key_value forKey:@"Facebook"];
    }
    [Cocoafish initializeWithOauthConsumerKey:consumer_key_value consumerSecret:secret_key_value customAppIds:appKeys];
    
    return self;
}

-(void) dealloc {
    [super dealloc];
}

- (id)linkWithFacebook:(id)args {
    [[Cocoafish defaultCocoafish] facebookAuth:[NSArray arrayWithObjects:@"publish_stream", @"email", @"offline_access", nil] delegate:nil];
}

- (id)apiCall:(id)args {
    
    ENSURE_UI_THREAD(apiCall,args);
    
    NSData *data = nil;
    CCRequest *request = nil;
    
    NSLog(@"[INFO] args %@ ", args);
    
    NSDictionary * params = [args objectAtIndex:0];
    NSDictionary * paramDict = [params objectForKey:@"params"];
    
    
    RELEASE_TO_NIL(successCallback);
    RELEASE_TO_NIL(errorCallback);
    
    successCallback = [[params objectForKey:@"success"] retain];
    errorCallback = [ [params objectForKey:@"error"] retain];
    
    
    // handle a photo
    if ([paramDict objectForKey:@"photo"] ) {
        
        id arg = [paramDict objectForKey:@"photo"];
        
        if ([arg isKindOfClass:[TiBlob class]])
        {
            TiBlob *blob = (TiBlob*)arg;
            [paramDict setValue:nil forKey:@"photo"];
            
            request = [[[CCRequest alloc] initWithDelegate:self httpMethod:[params objectForKey:@"httpMethod"] baseUrl:[params objectForKey:@"baseUrl"] paramDict:paramDict] autorelease];
            [request addPhotoUIImage:[blob image] paramDict:paramDict];
        } else {
            TiFile *file = (TiFile*)arg;
            NSString *mime = [Mimetypes mimeTypeForExtension:[file path]];
            if (mime == nil || [mime hasPrefix:@"image/"])
            {
                NSData *data = [NSData dataWithContentsOfFile:[file path]];
                UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
                
                [paramDict setValue:nil forKey:@"photo"];
                
                request = [[[CCRequest alloc] initWithDelegate:self httpMethod:[params objectForKey:@"httpMethod"] baseUrl:[params objectForKey:@"baseUrl"] paramDict:paramDict] autorelease];
                [request addPhotoUIImage:image paramDict:paramDict];

            } 
        }
    } else {
        request = [[[CCRequest alloc] initWithDelegate:self httpMethod:[params objectForKey:@"httpMethod"] baseUrl:[params objectForKey:@"baseUrl"] paramDict:paramDict] autorelease];
    }

    [request startAsynchronous];
}


#pragma mark -
#pragma mark CCRequest delegate methods
// successful logout
-(void)ccrequest:(CCRequest *)request didSucceed:(CCResponse *)response
{	
    NSLog(@"[INFO] ccrequest response %@ ", [response jsonResponse]);
    NSLog(@"[INFO] ccrequest meta %@ ", [response.meta description]);
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    [event setObject:[response jsonResponse] forKey:@"responseText"];
    [event setObject:[response jsonMeta] forKey:@"metaDataText"];
    
    
    
    if (successCallback)
    {
        [self _fireEventToListener:@"success" withObject:event listener:successCallback thisObject:nil];
    } else {
        [self fireEvent:@"success" withObject:event];
    }
    
    

    
}

-(void)ccrequest:(CCRequest *)request didFailWithError:(NSError *)error
{
    
    NSLog(@"[INFO] didFailWithError", nil);
    
	NSString *msg = [NSString stringWithFormat:@"%@.",[error localizedDescription]];
    
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    [event setObject:msg forKey:@"errorText"];
    
    if (errorCallback)
    {
        [self _fireEventToListener:@"error" withObject:event listener:errorCallback thisObject:nil];
    } else {
        [self fireEvent:@"error" withObject:event];
    }


}
@end