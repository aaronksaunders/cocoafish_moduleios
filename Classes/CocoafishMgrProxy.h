//
//  CocoafishProxy.h
//  cocoafishmodule
//
//  Created by Aaron Saunders on 12/23/11.
//  Copyright (c) 2011 Clearly Innovative Inc. All rights reserved.
//
//#import "TiModule.h"
#import "TiApp.h"
#import "Cocoafish.h"
#import <Foundation/Foundation.h>

@interface CocoafishMgrProxy : TiProxy {
@private
  TiApp * app;
  KrollCallback *successCallback;
  KrollCallback *errorCallback;
  NSString* consumer_key_value;
  NSString* secret_key_value;
  NSString* fb_key_value;
}

- (id)initWithTiApp:(TiApp *)tiApp keys:(NSDictionary *)keys;
@end

