// TODO: write your module tests here
var cocoafishMgr = require('com.ci.cocoafish');
Ti.API.info("module is => " + cocoafishMgr);

/*
 var cocoaFish = cocoafishMgr.create({
 "consumer_key" : "dIUKPhuQ9ZgoJgSFiJx92Fddz1svFBb4",
 "secret_key" : "kN0lem9ryJbnsYL1I4iauQgFRaVijPva"
 });
 */
var cocoaFish = cocoafishMgr.create({
	"consumer_key" : "dIUKPhuQ9ZgoJgSFiJx92Fddz1svFBb4",
	"secret_key" : "kN0lem9ryJbnsYL1I4iauQgFRaVijPva",
	"facebookAppId" : "307037819318849"
});
Ti.API.info("proxy is => " + cocoaFish);

function initWithTwitter() {
	var t = require('twitter');
	var tt = new t.twitter({
		service : 'twitter',
		consumer_key : "gLd5im6tpBEIiikB8UEanw",
		consumer_secret : "3fuoeSCgE28JhZCNhBNRXjKsDV9TAjuhm2Wr0VImP0"
	});

	//
	// since we were successful, we can get the parameters needed to
	// make the cocoafish api call to login/create external account.
	//
	// these configuration values are available from a customized
	// version of the birdhouse.js twitter module
	//
	var authorizationSuccess = function() {
		var config, param_data;
		config = tt.getConfig();
		Ti.API.info("access_token is => " + config.access_token);
		Ti.API.info("user_id is => " + config.user_id);
		Ti.API.info("screen_name is => " + config.screen_name);

		//
		// pass the params in following the cocoafish documentation
		var data = {
			type : "twitter",
			token : config.access_token,
			id : config.user_id
		};

//
// make the API call and you are done.
// 
// if the account exists already, then the user will be
// logged in
		cocoaFish.apiCall({
			"baseUrl" : "users/external_account_login.json",
			"httpMethod" : "POST",
			"params" : data,
			success : function(d) {
				Ti.API.info("responseText is => " + d.responseText);
				Ti.API.info("metaDataText is => " + d.metaDataText);
				var user = (JSON.parse(d.responseText)).response.users[0];
				Ti.API.info("user is => " + user);
			},
			error : function(d) {
				Ti.API.error("error is => " + d.errorText);
			}
		})
	}
	//
	// call to authorize with twitter account information. On success an account
	// will be created for you. In the future you can login with the same twitter
	// credentials
	tt.authorize(authorizationSuccess);
}

function loginWithFacebook() {
	var data = {
		type : "facebook",
		token : Titanium.Facebook.accessToken
	};

	cocoaFish.apiCall({
		"baseUrl" : "users/external_account_login.json",
		"httpMethod" : "POST",
		"params" : data,
		success : function(d) {
			Ti.API.info("responseText is => " + d.responseText);
			Ti.API.info("metaDataText is => " + d.metaDataText);
			var user = (JSON.parse(d.responseText)).response.users[0];
			getFacebookFriends()
		},
		error : function(d) {
			Ti.API.error("error is => " + d.errorText);
		}
	});
}

function initWithFacebook() {
	Titanium.Facebook.appid = "307037819318849";
	Titanium.Facebook.permissions = ['publish_stream'];
	Titanium.Facebook.addEventListener('login', function(e) {
		if(e.success) {
			loginWithFacebook();
		} else if(e.error) {
			_callback({
				success : false,
				response : e.error,
				error : e.error
			});
		} else if(e.cancelled) {
			_callback({
				success : false,
				response : "Cancelled",
				error : "Cancelled"
			});
		}
	});
	if(Titanium.Facebook.loggedIn === false) {
		Titanium.Facebook.authorize();
	} else {
		loginWithFacebook();
	}

	if(false) {
		cocoaFish.apiCall({
			"baseUrl" : "users/login.json",
			"httpMethod" : "POST",
			"params" : {
				"login" : "aaron@clearlyinnovative.com",
				"password" : "password"
			},
			success : function(d) {
				Ti.API.info("responseText is => " + d.responseText);
				Ti.API.info("metaDataText is => " + d.metaDataText);
				var user = (JSON.parse(d.responseText)).response.users[0];
				//getPhotos(user.id)
				linkWithFacebook();
			},
			error : function(d) {
				Ti.API.error("error is => " + d.errorText);
			}
		});

	}
}

function unlinkWithFacebook() {

	cocoaFish.apiCall({
		"baseUrl" : "users/external_account_unlink.json",
		"httpMethod" : "DELETE",
		"params" : {
			"type" : "facebook",
			"id" : Titanium.Facebook.uid,
		},
		success : function(d) {
			Ti.API.info("responseText is => " + d.responseText);
			Ti.API.info("metaDataText is => " + d.metaDataText);
		},
		error : function(d) {
			Ti.API.error("error is => " + d.errorText);
		}
	});

}

function getFacebookFriends() {
	Ti.API.info("in => getFacebookFriends");

	cocoaFish.apiCall({
		"baseUrl" : "social/facebook/search_friends.json",
		"httpMethod" : "GET",
		success : function(d) {
			Ti.API.info("responseText is => " + d.responseText);
			Ti.API.info("metaDataText is => " + d.metaDataText);
			unlinkWithFacebook();
		},
		error : function(d) {
			Ti.API.error("error is => " + d.errorText);
		}
	});

}

function getPhotos(user_id) {
	cocoaFish.apiCall({
		"baseUrl" : "photos/search.json",
		"httpMethod" : "GET",
		"params" : {
			"user_id" : user_id
		},
		success : function(d) {
			Ti.API.info("responseText is => " + d.responseText);
			Ti.API.info("metaDataText is => " + d.metaDataText);
			uploadPhoto(Ti.Filesystem.getFile("certificate.png"), {
				"tags" : "aaron, nativeIOS, fileObject"
			})
		},
		error : function(d) {
			Ti.API.error("error is => " + d.errorText);
		}
	});

}

/*
 * @params photo - can be either a blob or a file object
 * @params args - additional args/parameters to pass to API
 *
 */
function uploadPhoto(photo, args) {

	var _params = extend({}, args);

	Ti.API.debug("uploadPhoto " + typeof photo);

	_params.photo = photo;

	cocoaFish.apiCall({
		"baseUrl" : "photos/create.json",
		"httpMethod" : "POST",
		"params" : _params,
		"success" : function(d) {
			Ti.API.info("uploadPhoto: success is => " + d.responseText);
		},
		"error" : function(d) {
			Ti.API.info("uploadPhoto: error is => " + d.errorText);
		}
	});

}

//
// Create Global "extend" method
//
var extend = function(obj, extObj) {
	if(arguments.length > 2) {
		for(var a = 1; a < arguments.length; a++) {
			extend(obj, arguments[a]);
		}
	} else {
		for(var i in extObj) {
			obj[i] = extObj[i];
		}
	}
	return obj;
};
initWithTwitter();
